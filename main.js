const sermons = [
  {
    title: "Implicit Obedience",
    pastor: "Apostle J.C. Uka",
    text_content: `
      "The Biblical illustration of the miracle of Jesus Christ turning water into wine reveals unto us that implicit, complete, unquestioned and unprocrastinated obedience guarantees access to the new wine of the Holy Spirit of God. The Bible made us to understand that at the marriage in Cana of Galilee, they ran out of wine and Mary the mother of Jesus went to Jesus and said to Him “they have no wine”.
      Jesus answered and said to her “Woman, what have I to do with thee? Mine hour is not yet come”. That was not a favourable answer but Mary turned to the servants at the wedding and said to them: “Whatsoever he saith unto you do it.” Mary said in her heart: “I don’t take no as an answer. I know the key that opens the door for the miracle of the new wine. It is doing whatsoever He says you should do”. Then Jesus turned and spoke to the servants: “Fill the water pots with water.” Naturally, that sounds unreasonable; what finished was wine; what they were looking for is wine not water. The servants knew that the wine had finished yet they obeyed the Master and filled the water pots with water to the brim. Then Jesus commanded the servants: “Draw out now, and bear unto the governor of the feast” and they obeyed.  The servants obeyed without thinking of the embarrassments and the negative reactions from the men on the high table. The Bible made us to understand that before they got to the governor of the feast, the water turned into wine. The servants became carriers and distributors of the new wine. Today, God is still looking for servants who will do whatsoever he tells them to; servants who will become the kingdom carriers and distributors of the wine of the Holy Spirit.  In every generation, perfect and implicit obedience to God’s will guarantee constant flow of new wine of the Holy Spirit. 
      Charles G. Finney, a great revivalist with an unusual passion for lost souls, was a man who experienced revival and manifested the wine of the Holy Spirit in his time. He had one major secret in his life: that was his implicit obedience to God in every detail of his life. We are told that he went to get a young woman who was to become his wife. On his way, he stopped to have his horse shod, and while his horse was being shod, he felt persuaded in his spirit to preach the gospel in the adjacent church. Soon a crowd gathered and as he preached a revival broke out.  The Spirit of God came upon men and women and repentance and transformation took place. He and the entire city were stirred. The people   persuaded Finney to stay another night and preach again. He did so and the revival increased in power. After that they persuaded him to stay for the third night and again people got saved, conviction settled down upon the audience and the revival went on. So it became so great that Finney continued preaching night after night. Finally, realizing that he could not continue his journey, he engaged another man to go for his bride while he continued to preach the gospel. The revival continued for six months. Finney did not get away from there but was obedient to the Spirit of God. He put the things of God first. No wonder he was a man with an unusual manifestation of the power of the Holy Ghost. 
      Today, God is looking for those who will be willing to turn aside from their own plans and obey the Spirit of God; men and women who will be willing to break their own engagements in order to obey God. Finney experienced great revival in his days. He was sold out to God and very obedient at every point, hence, God used him for His glory and honour. 
      The questions I want you to consider are:
      •	Are you doing the will of God?
      •	Do you turn away when God tells you to turn away?
      •	Do you do the will of God when it is revealed unto you?
      •	Are you living moment by moment in the center of His will?
      •	Do you listen and obey the voice of the Holy Spirit?
      The new wine of the Holy Spirit is for those sold out believers in Christ Jesus, who are ready to obey God at every point in their lives and destiny.",
    `,
    audio_or_video_url: "",
    background_image: "",
    date: ""
  },
  {
    title: "God needs leaders",
    pastor: "Samuel Affiah",
    text_content: `
      "
      Our community, nation and the world are faced with problems that appear to be insurmountable. There are security and defence challenges which are escalating at an alarming rate.Our youths, our future leaders are confused and demoralized. Morals are at an all-time low. Moral standards are almost nonexistent. The economic conditions of most nations are pitiable and unstable - growing national debts, bankrupt nations etc. Apart from all these problems, our generation is facing a serious problem: a leadership crisis.
      Jesus, during his assignment here on earth observed that the multitude around him were helpless and harassed. He also seems to proffer a solution to their situation:
      Jesus went through all the towns and villages, teaching in their synagogues, preaching the good news of the kingdom and healing every disease and sickness.
      When he saw the crowds, he had compassion on them, because they were harassed and helpless, like sheep without a shepherd.
      Then he said to his disciples, "The harvest is plentiful but the workers are few.
      Ask the Lord of the harvest, therefore, to send out workers into his harvest field." 
      Matthew 9:35-38(NIV)
      From this scripture, we can see the way God feels about our nation and the world:
      (I) God loves (compassion) the world. He has compassion on the people. God is concerned. The Message version uses “His heart broke” in the place of “Compassion”
      (ii) God sees the world as being confused, aimless, harassed, worried and helpless.
      (iii) He says that the “harassed” and “helpless” world need shepherds (Leaders to guide and give direction)
      It is safe to conclude that leaders are very important in God’s agenda for transformation. God needs leaders.
      
      "
    `,
    audio_or_video_url: "",
    background_image: "",
    date: ""
  },
  {
    title: "Enthrone him as king",
    pastor: "Pastor Samuel Affiah",
    text_content: "",
    audio_or_video_url: {
      $ref: "#/audio/Enthrone-Him-As-King-Pastor-Samuel-Affiah.mp3"
    },
    background_image: "",
    date: ""
  },
  {
    title: "How does God see you",
    pastor: "Pastor Mrs Delphine Uka",
    text_content: `
      One morning I was reading my bible precisely 1st Kings 11, where Solomon loved and married strange women, went after their gods and built high places for their gods. I asked God, I said: “Lord why did you keep quiet when you saw that Solomon’s heart was shifting away from you? Why didn’t you warn Him?” Then I heard him say “My daughter”. Wao! I was in tears for hearing Him call me His daughter again after a long time. I had to kneel down and thank him for His mercy on this backslidden child of His. I am sure you will be wondering why? 
      For some time (a long time) God had not used the word “daughter” for me. Once he was speaking about me to my husband and referred to me as “Your wife”. My husband didn’t understand that it was a great blow on me. God had moved from the level of being “His own daughter” to being “Your wife”. I knew I must have placed God second after my husband. I cried so much in my heart and kept asking God to restore me back to the state of being His daughter.
      Many of us today have moved from being God’s sons or daughters to being the mother or father of so and so, the manager or MD of business so and so, etc. without even being conscious of it. You may still be praying, reading/studying your bible, attending services, carrying out your duties in church but before God, you have put other things before Him. Check it out.
      Now back to my narration. When I heard him say: “My daughter”, I didn’t even wait to hear whatever else He had to say, I was broken; on my knees crying and thanking Him for His mercy. He said: “I do not punish without giving adequate warning.” He told me to read on. I saw that God had appeared to Solomon two (2) times and warned him, Himself. He didn’t send any prophet but warned Solomon Himself. He said: “You people say that I am not a talkative and that is true. I warn but if despite the warnings you decide to do the exact things I warned you against, and also refuse to listen to the voice of your conscience warning you, then I won’t stop you. I will allow you go to the end and then face the consequences like Solomon.” See God’s verdict:  
        1 Kings 11:11 (NIV)
      11 So the LORD said to Solomon, "Since this is your attitude and you have not kept my covenant and my decrees, which I commanded you, I will most certainly tear the kingdom away from you and give it to one of your subordinates. 
      May this verdict not be our portion in Jesus name? Let us examine our lives. What position have we given God? Have we placed our jobs, businesses, education, spouses, kids, concern of the family before God?
      Jesus said in Matthew 10:37-39     
      37 He who loves father or mother more than Me is not worthy of Me. And he who loves son or daughter more than Me is not worthy of Me.  38 And he who does not take his cross and follow after Me is not worthy of Me.  39 He who finds his life will lose it, and he who loses his life for My sake will find it. 
      Do you fall into this category? You always have a reason for not going for fellowship, despite the warnings that we should nit forsake the assembly of ourselves together, as the manner of some is:  but exhorting one another: so much the more, as ye see the day approaching (Hebrews 10:25)
      Don’t ever expect God or His angels to carry a cane pursuing you to do what you have to do. God must be Number 1. He cannot share that position with any other. 
      My brother and sister, examine yourself – Is God still Number 1? What are the contents of your prayer?
      There is a prayer I pray from time to time: “God, how do you see me?” “How does Heaven perceive me?” I never heard any response but today, I got it. May God by His mercy help me maintain it. 
      God by His mercy will restore you to your original state of Sonship in Jesus name.
      Shalom!    
    `,
    audio_or_video_url: "",
    background_image: "",
    date: ""
  },
  {
    title: "God make me a voice",
    pastor: "Apostle J.C. Uka",
    text_content: `
      If God makes you a voice, when you speak, sicknesses and diseases will disappear. You speak with authority and demons tremble. The same God that ordained the voice of Elijah, He is the same forever and He will be on your side. 
    `,
    audio_or_video_url: "audioGOD MAKE ME A VOICE - Apostle J.C. Uka.mp3",
    background_image: "",
    date: ""
  },
  {
    title: "Jonah in the boat of your life",
    pastor: "Apostle J.C. Uka",
    text_content: `
      It is an expensive joke to carry Jonah when you are working for the Lord. When you want to fulfil the assignment God has given to you, you must make sure that whatever represents Jonah will not be in the boat of your ministry, family or your life.
    "
    `,
    audio_or_video_url:
      "websiteaudioJONAH INTHE BOAT OF YOUR LIFE - Apostle J.C. Uka.mp3",
    background_image: "",
    date: ""
  },
  {
    title: "O Lord remember me",
    pastor: "Apostle J.C. Uka",
    text_content: `
      God is faithful and a God that keeps His covenant. When God remember you He delivers you from every form of barrenness and stagnation.
    `,
    audio_or_video_url: "websiteaudioO LORD REMEMBER ME - Apostle J.C. Uka.mp3",
    background_image: "",
    date: ""
  },
  {
    title: "Principles of kingdom marriage",
    pastor: "Apostle J.C. Uka",
    text_content: `
      Marriage is the mind of God.
      When the marriage is not in order, we will not be able to produce people that will carry out the mind of God.    
    `,
    audio_or_video_url:
      "websiteaudioPrinciples of Kingdom Marriage - Apostle J.C. Uka.mp3",
    background_image: "",
    date: ""
  },
  {
    title: "God will do a new thing",
    pastor: "Apostle J.C. Uka",
    text_content: `
      God will do a new thing is one of many prophetic messages by Apostle J.C. Uka. This message talks about God having the ability to do new things in the lives of people, families and nations.
    `,
    audio_or_video_url: "audioGOD WILL DO A NEW THING - Apostle J.C. Uka.mp3",
    background_image: "",
    date: ""
  },
  {
    title: "",
    pastor: "",
    text_content: "",
    audio_or_video_url: "",
    background_image: "",
    date: ""
  }
];

const ages = [33, 21, 22, 3, 4, 54, 12, 3, 13, 15, 61, 44];
//1. for loop
/*for( var i =0; i < sermons.length; i++){
    console.log(i, sermons[i].title);
}

//2. forEach
//arow function expression
/*sermons.forEach(sermon => {
    console.log(sermon);
});*/

/*sermons.forEach(function(sermon){
    console.log(sermon);
})*/

//3. filter

/*const canDrink = ages.filter(age=>{
    if( age >= 21){
        return true;
    }
}); */

//or

const canDrink = ages.filter(age => age >= 21);

console.log(canDrink);

/*let canDrink = [];
ages.forEach(function(age){
    if( age >= 21){
        canDrink.push(age);
    }
})
console.log(canDrink)*/

const sermonsByPastorUka = sermons.filter(
  sermon => sermon.pastor == "Apostle J.C. Uka"
);
console.log(sermonsByPastorUka);

//4. map
const sermonsTitles = sermons.map(sermon => sermon.title);
/*

//equivalent using forEach
const sermonsTitles = [];

sermons.forEach(sermon => {
    sermonsTitles.push(sermon.title);
})
*/
console.log("Sermon Titles", sermonsTitles);

const sermonsByPastors = sermons.map(
  sermon => ` ${sermon.title} by [${sermon.pastor}]`
);
console.log(sermonsByPastors);

const dob = ages.map(age => "You were born in the year " + (2020 - age));
console.log(dob);

const squares = [1, 25, 36, 49, 64, 4, 9, 16, 81, 100, 144, 121];

const squaresMap = squares
  .map(square => Math.sqrt(square))
  .map(square => square * 2)
  .map(square => square / 2);

console.log(squaresMap);

//const sortedSquareArray = squares.sort((a, b) => a > b ? 1 : -1);
//const sortedSquareArray = squares.sort((a, b) => a-b);
const sortedSquareArray = squares.sort((a, b) => b - a);

console.log(sortedSquareArray);

//5. Reduce
//let squaresSum = 0;
//using map
//squaresSum = squares.map(square => ( squaresSum = squaresSum + square));
//console.log(squaresSum[squares.length -1]);

//using for loop
//for(i = 0; i < squares.length; i++){
//   squaresSum += squares[i];
//}

//Reduce

const squaresSum = squares.reduce((total, square) => total + square, 0);
console.log(squaresSum);

//combine map, filter, sort and reduce

const combined = squares
  .map(square => Math.sqrt(square))
  .map(square => square * square)
  .filter(square => square > 0)
  .sort((a, b) => a - b)
  .reduce((total, square) => total + square, 0);
console.log(combined);

userChoices = ["rock", "paper", "scissors", "bomb"];

const getUserChoice = userInput => checkInput(userInput);

const checkInput = userInput =>
  userChoices.indexOf(userInput.toLowerCase()) !== -1
    ? userInput.toLowerCase()
    : "Error";

//console.log(getUserChoice('Rock'))

const getComputerChoice = () => userChoices[Math.floor(Math.random(0, 1) * 3)];

//console.log(getComputerChoice());

const determineWinner = (userChoice, computerChoice) => {
  if (userChoice == computerChoice) {
    return "The game was a tie!";
  } else {
    if (userChoice == "rock") {
      if (computerChoice == "paper") {
        return "The computer won!";
      } else {
        return "The user won!";
      }
    }
    if (userChoice == "paper") {
      if (computerChoice == "scissors" || computerChoice == "rock") {
        return "Oluwaseun has won!";
      } else {
        return "No winner!";
      }
    }
    if (userChoice == "scissors") {
      if (computerChoice == "rock" || computerChoice == "paper") {
        return "Victoria has won!";
      } else {
        return "Inconclusive!";
      }
    }
    if (userChoice == "bomb") {
      return "The user has won the game!";
    }
  }
};

//console.log(determineWinner('scissors', 'rock'));

const playGame = () => {
  let userChoice = getUserChoice("bomb");
  let computerChoice = getComputerChoice();
  console.log(userChoice, computerChoice);
  console.log(determineWinner(userChoice, computerChoice));
};

playGame();

const days = [
  "sunday",
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday"
];

const hoursOfSleep = [4, 5, 6, 7, 6, 5, 7];
const getSleepHours = day => {
  day = day.toLowerCase();
  if (days.indexOf(day) !== -1) {
    return hoursOfSleep[days.indexOf(day)];
  } else {
    return "404";
  }
};
console.log(getSleepHours("friDay"));

const getActualSleepHours = () => {
  let totalSleepHours = 0;
  for (let i = 0; i < days.length; i++) {
    totalSleepHours += getSleepHours(days[i]);
  }
  return totalSleepHours;
};

//hoursOfSleep.reduce((total, hours) =>{
//return total + hours;
//});

console.log(getActualSleepHours());

const getIdealSleepHours = () => {
  var idealHours = 8;
  return idealHours * 7;
};

console.log(getIdealSleepHours());

const calculateSleepDebt = () => {
  let actualSleepHours = getActualSleepHours();
  let idealSleepHours = getIdealSleepHours();
  if (actualSleepHours == idealSleepHours) {
    return console.log("You got the perfect anmount of sleep.");
  }

  if (actualSleepHours > idealSleepHours) {
    return console.log("You got more sleep than needed");
  }

  if (actualSleepHours < idealSleepHours) {
    return console.log("You should get some rest");
  }
};

calculateSleepDebt();

let spaceship = {
  crew: {
    captain: {
      name: "Lily",
      degree: "Computer Engineering",
      cheerTeam() {
        console.log("You got this!");
      }
    },
    "chief officer": {
      name: "Dan",
      degree: "Aerospace Engineering",
      agree() {
        console.log("I agree, captain!");
      }
    },
    medic: {
      name: "Clementine",
      degree: "Physics",
      announce() {
        console.log(`Jets on!`);
      }
    },
    translator: {
      name: "Shauna",
      degree: "Conservation Science",
      powerFuel() {
        console.log("The tank is full!");
      }
    }
  }
};

// Write your code below
for (let crewMember in spaceship.crew) {
  console.log(`${crewMember}: ${spaceship.crew[crewMember].name}`);
}

for (let crewMember in spaceship.crew) {
  console.log(`${crewMember}: ${spaceship.crew[crewMember].degree}`);
}

/*const robot = {
  _model: "1E78V2",
  _energyLevel: 100,
  get energyLevel() {
    if (typeof this._energyLevel == "number") {
      return `My current energy level is ${this._energyLevel}`;
    } else {
      return "System malfunction: cannot retrieve energy level";
    }
  }
};

console.log(robot.energyLevel);
*/
const robot = {
  model: "SAL-1000",
  mobile: true,
  sentient: false,
  armor: "Steel-plated",
  energyLevel: 75
};

// What is missing in the following method call?
const robotKeys = Object.keys(robot);

console.log(robotKeys);

// Declare robotEntries below this line:
const robotEntries = Object.entries(robot);

console.log(robotEntries);

// Declare newRobot below this line:
const newRobot = Object.assign(
  { laserBlaster: true, voiceRecognition: true },
  robot
);

console.log(newRobot);

let profit = 0;
let addition = 0;
function calculateMoney(p = 0, amount, durationInMonths) {
  for (i = 0; i < durationInMonths; i++) {
    profit = (p / 100) * amount;
    amount = amount + profit;
    console.log(i + 1, profit, amount);
  }
  return amount;
}

console.log(calculateMoney(20, 100000, 2));

function addMoreMoney(addition, calculateMoney) {
  let pamount = 208800;
  let sum = addition + pamount;
  let amt = calculateMoney(20, sum, 2);

  return amt;
}

console.log(addMoreMoney(30000, calculateMoney));

const menu = {
  _courses: {
    _appetizers: [],
    _mains: [],
    _desserts: [],
    get appetizers() {
      return this._appetizers;
    },
    set appetizers(appetizer) {
      this._appetizers = appetizer;
    },
    get mains() {
      return this._mains;
    },
    set mains(main) {
      this._mains = main;
    },
    get desserts() {
      return this._desserts;
    },
    set desserts(dessert) {
      this._desserts = dessert;
    }
  },
  get courses() {
    return {
      appetizers: this._courses.appetizers,
      mains: this._courses.mains,
      desserts: this._courses.desserts
    };
  },
  addDishToCourse(courseName, dishName, dishPrice) {
    const dish = {
      name: dishName,
      price: dishPrice
    };
    this._courses[courseName].push(dish);
  },
  getRandomDishFromCourse(courseName) {
    const dishes = this.courses[courseName];
    const randomIndex = Math.floor(Math.random() * dishes.length);
    return dishes[randomIndex];
  },
  generateRandomMeal() {
    const appetizer = this.getRandomDishFromCourse("appetizers");
    const main = this.getRandomDishFromCourse("mains");
    const dessert = this.getRandomDishFromCourse("desserts");
    const totalPrice = appetizer.price + main.price + dessert.price;

    return `Your appetizer is ${appetizer.name}, Your main course is ${main.name}, Your dessert is ${dessert.name} and the total price is ${totalPrice}`;
    //return this.courses;
  }
};

menu.addDishToCourse("appetizers", "fries", 20.5);
menu.addDishToCourse("appetizers", "wings", 10.5);
menu.addDishToCourse("appetizers", "garri", 5.5);

menu.addDishToCourse("mains", "abula", 55.5);
menu.addDishToCourse("mains", "fried rice and chicken", 25.5);
menu.addDishToCourse("mains", "yam porridge", 15.5);

menu.addDishToCourse("desserts", "ice cream", 5.5);
menu.addDishToCourse("desserts", "coke", 5.5);
menu.addDishToCourse("desserts", "milo", 5.5);

const meal = menu.generateRandomMeal();
console.log(meal);

class Surgeon {
  constructor(name, department) {
    this._name = name;
    this._department = department;
    this._remainingVacationDays = 20;
  }

  get name() {
    return this._name;
  }

  get department() {
    return this._department;
  }

  get remainingVacationDays() {
    return this._remainingVacationDays;
  }

  takeVacationDays(daysOff) {
    return this.remainingVacationDays - daysOff;
  }
}

class HospitalEmployee {
  constructor(name) {
    this._name = name;
    this._remainingVacationDays = 20;
  }

  get name() {
    return this._name;
  }

  get remainingVacationDays() {
    return this._remainingVacationDays;
  }

  takeVacationDays(daysOff) {
    this._remainingVacationDays -= daysOff;
  }
  static generatePassword() {
    return Math.floor(Math.random() * 10000);
  }
}

class Nurse extends HospitalEmployee {
  constructor(name, certifications) {
    super(name);
    this._certifications = certifications;
  }

  get certifications() {
    return this._certifications;
  }

  addCertification(newCertification) {
    this.certifications.push(newCertification);
  }
}

const nurseOlynyk = new Nurse("Olynyk", ["Trauma", "Pediatrics"]);
nurseOlynyk.takeVacationDays(5);
console.log(nurseOlynyk.remainingVacationDays);
nurseOlynyk.addCertification("Genetics");
console.log(nurseOlynyk.certifications);

class Media {
  constructor(title) {
    this._title = title;
    this._isCheckedOut = false;
    this._ratings = [];
  }
  get title() {
    return this._title;
  }
  get isCheckedOut() {
    return this._isCheckedOut;
  }
  get ratings() {
    return this._ratings;
  }
  set isCheckedOut(value) {
    this._isCheckedOut = value;
  }
  toggleCheckOutStatus() {
    return (this._isCheckedOut = !this._isCheckedOut);
  }

  getAverageRating() {
    return (
      this.ratings.reduce((accumulator, current) => accumulator + current) /
      this.ratings.length
    ).toFixed(2);
  }
  addRating(value) {
    return this._ratings.push(value);
  }
}

class Book extends Media {
  constructor(author, title, pages) {
    super(title);
    this._author = author;
    this._pages = pages;
  }
  get author() {
    return this._author;
  }
  get pages() {
    return this._pages;
  }
}

class Movie extends Media {
  constructor(director, title, runTime) {
    super(title);
    this._director = director;
    this._runtime = runTime;
  }
  get director() {
    return this._director;
  }
  get runTime() {
    return this._runtime;
  }
}
const historyOfEverything = new Book(
  "Bill Bryson",
  "A Short History of Nearly Everything",
  544
);
console.log(historyOfEverything);
historyOfEverything.toggleCheckOutStatus();
console.log(historyOfEverything.isCheckedOut);
historyOfEverything.addRating(4);
historyOfEverything.addRating(5);
historyOfEverything.addRating(5);
console.log(historyOfEverything.ratings);

const speed = new Movie("Jan de Bont", "Speed", 116);
console.log(speed);
speed.toggleCheckOutStatus();
console.log(speed.isCheckedOut);
speed.addRating(1);
speed.addRating(1);
speed.addRating(5);
console.log(speed.getAverageRating());

class School {
  constructor(name, level, numberOfStudents) {
    this._name = name;
    this._level = level;
    this._numberOfStudents = numberOfStudents;
  }
  get name() {
    return this._name;
  }
  get level() {
    return this._level;
  }
  get numberOfStudents() {
    return this._numberOfStudents;
  }
  set numberOfStudents(value) {
    if (typeof value === "number") {
      this.numberOfStudents = value;
    } else {
      return `Invalid input: numberOfStudents must be set to a Number.`;
    }
  }
  quickFacts() {
    console.log(
      `${this.name} educates ${this.numberOfStudents} students at the ${this.level} school level.`
    );
  }
  static pickSubstituteTeacher(substituteTeachers) {
    const randomIndex = Math.floor(Math.random() * substituteTeachers.length);
    //console.log(substituteTeachers);
    return substituteTeachers[randomIndex];
  }
}

class PrimarySchool extends School {
  constructor(name, numberOfStudents, pickupPolicy) {
    super(name, "primary", numberOfStudents);
    this._pickupPolicy = pickupPolicy;
  }
  get pickupPolicy() {
    return this._pickupPolicy;
  }
}

class HighSchool extends School {
  constructor(name, numberOfStudents, sportsTeams) {
    super(name, "highSchool", numberOfStudents);
    this._sportsTeams = sportsTeams;
  }
  get sportsTeams() {
    return this._sportsTeams.map(s => s);
  }
}

const lorraineHansburry = new PrimarySchool(
  "Lorraine Hansbury",
  514,
  "Students must be picked up by a parent, guardian, or a family member over the age of 13."
);
console.log(lorraineHansburry);
lorraineHansburry.quickFacts();

const substituteHans = School.pickSubstituteTeacher([
  "Jamal Crawford",
  "Lou Williams",
  "J. R. Smith",
  "James Harden",
  "Jason Terry",
  "Manu Ginobli"
]);
console.log(substituteHans);

const alSmith = new HighSchool("Al E. Smith", 415, [
  "Baseball",
  "Basketball",
  "Volleyball",
  "Track and Field"
]);

console.log(alSmith.sportsTeams);
